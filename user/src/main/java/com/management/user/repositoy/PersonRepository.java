package com.management.user.repositoy;

import com.management.user.data.Person;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PersonRepository extends JpaRepository<Person,Integer> {

    List<Person> findAll();

    Person findAllById(Integer id);

    Person findAllByEmail(String email);

    Person findAllByName(String personName);
}
