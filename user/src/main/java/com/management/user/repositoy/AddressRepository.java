package com.management.user.repositoy;

import com.management.user.data.Address;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AddressRepository extends JpaRepository<Address,Integer> {

}
