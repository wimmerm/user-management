package com.management.user.data;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "persons")
public class Person {

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private int id;

    private String name;
    private String email;
    private String password;
    private int salary;

    @OneToMany(cascade = CascadeType.ALL,mappedBy="person")
    private List<Address> addresses;

    @ManyToMany(cascade=CascadeType.ALL)
    private List<Group> groupList;

    //default Constructor
    public Person() {
    }


    public Person(Person person) {
        this.name = person.getName();
        this.email = person.getEmail();
        this.password = person.getPassword();
        this.salary = person.getSalary();
        this.addresses = person.getAddresses();
        this.groupList = person.getGroupList();
        this.id = person.getId();
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public List<Group> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<Group> groupList) {
        this.groupList = groupList;
    }
}
