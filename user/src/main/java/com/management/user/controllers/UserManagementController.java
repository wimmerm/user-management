package com.management.user.controllers;



import com.management.user.dto.PersonDTO;
import com.management.user.serevice.PersonDTOMapper;
import com.management.user.serevice.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/user-management/")
public class UserManagementController {

    @Autowired
    UserManagementService userManagementService;

    @Autowired
    PersonDTOMapper personDTOMapper;

    @GetMapping("/kotlar/find/{id}")
    public PersonDTO findPersonId(@PathVariable("id") Integer id){
        return personDTOMapper.mapToDTO(userManagementService.findPersonById(id));
    }

    @PreAuthorize("hasAnyRole('Manager')")
    @GetMapping("/kotlar/manager/{email}")
    public PersonDTO findPersonByEmail(@PathVariable("email")String email){
        return personDTOMapper.mapToDTO(userManagementService.findPersonByEmail(email));
    }
}
