package com.management.user.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.management.user.data.Address;
import com.management.user.data.Group;

import java.util.List;

public class PersonDTO {

    private int id;
    private String name;
    private String email;
    @JsonIgnore
    private String password;
    @JsonIgnore
    private int salary;
    private List<Address> addresses;
    private List<Group> groupList;

    public PersonDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<Group> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<Group> groupList) {
        this.groupList = groupList;
    }
}
