package com.management.user.resource;

import com.management.user.data.Address;
import com.management.user.data.Group;
import com.management.user.data.Person;
import com.management.user.data.Role;
import com.management.user.repositoy.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;

@Component
public class DataLoader implements ApplicationRunner {

    @Autowired
    PersonRepository personRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Person person1 = new Person();
        Person person2 = new Person();
        Person person3 = new Person();
        Person person4 = new Person();
        Person person5 = new Person();


        Address address = new Address();
        Address address1 = new Address();
        Address address2 = new Address();
        Address address3 = new Address();
        Address address4 = new Address();

        Group group1 = new Group();
        Group group2 = new Group();
        Group group3 = new Group();
        Group group4 = new Group();
        Group group5 = new Group();

        Role role1 = new Role();
        Role role2 = new Role();
        Role role3 = new Role();
        Role role4 = new Role();
        Role role5 = new Role();

        person1.setName("test");
        person1.setEmail("test@test.cz");
        person1.setPassword("15151");
        person1.setSalary(15165);
        person1.setAddresses(Arrays.asList(address, address1));
        person1.setGroupList(Arrays.asList(group1, group2));

        person3.setName("Marek");
        person3.setEmail("marek@marek.cz");
        person3.setPassword("1234");
        person3.setSalary(0);
        person3.setAddresses(Arrays.asList(address1, address3));
        person3.setGroupList(Arrays.asList(group2, group1));

        person4.setName("Marekk");
        person4.setEmail("marek@marek.cz");
        person4.setPassword("1234");
        person4.setSalary(0);
        person4.setAddresses(Arrays.asList(address1, address4));
        person4.setGroupList(Arrays.asList(group2, group5));

        person2.setName("Marek22");
        person2.setEmail("marek@marek.cz");
        person2.setPassword("1234");
        person2.setSalary(0);
        person2.setAddresses(Arrays.asList(address1, address2));
        person2.setGroupList(Arrays.asList(group2, group4));

        person5.setName("Marek33");
        person5.setEmail("marek@marek.cz");
        person5.setPassword("1234");
        person5.setSalary(0);
        person5.setAddresses(Arrays.asList(address1, address3));
        person5.setGroupList(Arrays.asList(group2, group5));

        address.setCity("Prahaff");
        address.setCountry("cz");
        address.setPostcode(19900);
        address.setStreet("test");
        address.setPerson(person1);

        address1.setCity("Prahdda");
        address1.setCountry("cz");
        address1.setPostcode(19900);
        address1.setStreet("test");
        address1.setPerson(person2);

        address2.setCity("Prahfffa");
        address2.setCountry("cz");
        address2.setPostcode(19900);
        address2.setStreet("test");
        address2.setPerson(person3);

        address3.setCity("Pradfsfha");
        address3.setCountry("cz");
        address3.setPostcode(19900);
        address3.setStreet("test");
        address3.setPerson(person4);

        address4.setCity("Pffraha");
        address4.setCountry("cz");
        address4.setPostcode(19900);
        address4.setStreet("test");
        address4.setPerson(person5);

        group1.setDescription("test");
        group1.setName("Test");
        group1.setExpirationDate(LocalDate.now().plusMonths(5).toString());
        group1.setPersonList(Arrays.asList(person1));
        group1.setRoleList(Arrays.asList(role5, role1));


        group2.setDescription("test");
        group2.setName("Test");
        group2.setPersonList(Arrays.asList(person2, person1));
        group2.setRoleList(Arrays.asList(role2, role5));
        group2.setExpirationDate(LocalDate.now().plusMonths(6).toString());

        group3.setDescription("test3");
        group3.setName("Test");
        group3.setPersonList(Arrays.asList(person4, person5));
        group3.setRoleList(Arrays.asList(role2, role3));
        group3.setExpirationDate(LocalDate.now().plusMonths(6).toString());

        group4.setDescription("test4");
        group4.setName("Test");
        group4.setPersonList(Arrays.asList(person2, person4));
        group4.setRoleList(Arrays.asList(role3, role4));
        group4.setExpirationDate(LocalDate.now().plusMonths(6).toString());

        group5.setDescription("test5");
        group5.setName("Test");
        group5.setPersonList(Arrays.asList(person3, person5));
        group5.setRoleList(Arrays.asList(role4, role5));
        group5.setExpirationDate(LocalDate.now().plusMonths(6).toString());

        role1.setDescription("Role test");
        role1.setRoleType("Manager");
        role1.setGroupList(Arrays.asList(group1, group2));

        role2.setDescription("test 2");
        role2.setRoleType("Owner");
        role2.setGroupList(Arrays.asList(group2, group3));

        role3.setDescription("test 2");
        role3.setRoleType("Developer");
        role3.setGroupList(Arrays.asList(group3, group4));

        role4.setDescription("test 2");
        role4.setRoleType("Employee");
        role4.setGroupList(Arrays.asList(group4, group5));

        role5.setDescription("test 2");
        role5.setRoleType("Friend");
        role5.setGroupList(Arrays.asList(group5, group1));

        personRepository.save(person1);
        personRepository.save(person3);
        personRepository.save(person4);
        personRepository.save(person2);
        personRepository.save(person5);

    }
}
