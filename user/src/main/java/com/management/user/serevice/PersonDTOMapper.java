package com.management.user.serevice;

import com.management.user.data.Person;
import com.management.user.dto.PersonDTO;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring")
public interface PersonDTOMapper {

    PersonDTO mapToDTO (Person person);
    Person mapFromDTO(PersonDTO personDTO);

}
