package com.management.user.serevice;

import com.management.user.data.CustomPerson;
import com.management.user.data.Person;
import com.management.user.repositoy.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyPersonService implements UserDetailsService {

    @Autowired
    PersonRepository personRepository;

    @Override
    public UserDetails loadUserByUsername(String personName) throws UsernameNotFoundException {
        Person person = personRepository.findAllByName(personName);
        return new CustomPerson(person);
    }
}
