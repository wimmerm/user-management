package com.management.user.serevice;

import com.management.user.data.Person;
import com.management.user.repositoy.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserManagementService {


    @Autowired
    PersonRepository personRepository;



    public Person findPersonById(int id){

        return personRepository.findAllById(id);
    }

    public Person findPersonByName(String name){

        return personRepository.findAllByName(name);
    }

    public Person findPersonByEmail(String email){

        return personRepository.findAllByName(email);
    }


}
